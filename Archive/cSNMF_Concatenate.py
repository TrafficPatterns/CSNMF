''' This contains functions for cSNMF.'''

import numpy as np
import pandas as pd
import math
import logging
import itertools as it
import config

logger = logging.getLogger(__name__)


## Useful matrix operations:
def m(A,B): return np.multiply(A,B)
def d(A): return np.diag(np.diag(A))  # d(A) replaces all off-diagonal entries of A with 0.
def N(A): return m(A, NONZEROS)       # NONZEROS is a global variable.
def N1(A): return m(A, np.concatenate((NONZEROS, np.ones((1, len(NONZEROS.T))))))                 # For D1
def N2(A): return m(A, np.concatenate((NONZEROS, np.ones((len(NONZEROS), config.RANK))), axis=1)) # For D2


def sparsity_metric(H):
    def sparsity_column(column):
        n = len(column)
        ratio = np.linalg.norm(column, 1)/np.linalg.norm(column, 2)
        return (math.sqrt(n) - ratio)/(math.sqrt(n) - 1)
    return np.mean([sparsity_column(column) for column in H.T])


def calculate_error(D, W, H):
    norm_D = np.linalg.norm(D)
    return np.linalg.norm(D - N(W@H))/norm_D*100


def impose_L1_constraint(W, H):
    W2 = W/sum(W)
    H2 = np.array([H[i]*sum(W)[i] for i in range(len(H))])
    return W2, H2



def axe_H(H, tail_cutoff = 0.4):
    H2 = H.copy()
    for i, column in enumerate(H.T):
        sorted_column = sorted(column)
        cumulative_sum = it.accumulate(sorted_column)
        kill_count = sum(list(cumulative_sum) < tail_cutoff*sum(column))
        H2.T[i] = np.array([entry if entry >= sorted_column[kill_count] else 0 for entry in column])
    return H2


def sort_WH(W, H):
    H2 = axe_H(H)
    weights = H2/sum(H2)
    signature_popularities = sum(weights.T)
    signature_popularities = sorted(enumerate(signature_popularities), key = lambda x : x[1], reverse = True)
    W2 = W.copy()
    H3 = H2.copy()
    for i in range(len(signature_popularities)):
        W2[:, i] = W[:, signature_popularities[i][0]]
        H3[i] = H2[signature_popularities[i][0]]
    try:
        assert ( np.linalg.norm(W@H2 - W2@H3) / np.linalg.norm(W@H2) ) < 0.01
    except AssertionError:
        log.critical('Sorting is not working correctly')
        return W2, H3
    
    return W2, H3



def factorize(data_array,
              rank = config.RANK,
              beta = None,
              threshold = 0.5,
              max_iter = 600,
              seed_W = None,
              seed_H = None,
              log = logger,
              debug = False,
              axing = True):

    log.info('Rank= %s, Threshold= %s', rank, threshold)

    D = np.nan_to_num(data_array)
    eta = D.max()
    if beta is None:
        beta = len(D.T)//2
    global NONZEROS
    NONZEROS = ~np.isnan(data_array)
    
    logger.info('Initializing W and H...')
    
    W_shape = (D.shape[0], rank)
    H_shape = (rank, D.shape[1])
    
    np.random.seed(seed_W)
    W = np.random.uniform(low = 0.01, high = 1., size = W_shape)    
    np.random.seed(seed_H)
    H = np.random.uniform(low = 0.01, high = 1., size = H_shape)
    log.info('W, H chosen')

    ##### Adding concatenation here:
    D1 = np.concatenate((D, np.zeros((1, H_shape[1]))))
    assert D1.shape == (W_shape[0] + 1, H_shape[1])
    D2 = np.concatenate((D, np.zeros(W_shape)), axis=1)
    assert D2.shape == (W_shape[0], H_shape[1] + W_shape[1])
    
    
    def update_W(W, H):
        H1 = np.concatenate((H, eta**0.5*np.eye(H_shape[0])), axis=1)
        assert H1.shape == (H_shape[0], H_shape[1] + H_shape[0])
    
        Term_1 = D2 @ H1.T 
        Term_2 = N2(W @ H1) @ H1.T
        W = m(W, Term_1)/Term_2
        return W

    
    def update_H(W, H):
        W1 = np.concatenate((W, beta**0.5*np.ones((1, W_shape[1]))))
        assert W1.shape == (W_shape[0] + 1, W_shape[1])
        
        Term_3 = W1.T @ D1
        Term_4 = W1.T @ N1(W1 @ H)
        H = m(H, Term_3)/Term_4
        return H
    

    def quartiles(H):
        H2 = sorted(H.flatten())
        quarter = (rank*D.shape[1]-1)//4
        return H2[0], H2[quarter], H2[quarter*2], H2[quarter*3], H2[-1]

    

    iterations = 0
    column_names = ['error', 'sparsity', 'diff_W', 'diff_H', 'W_minmax',
                    'H_0th', 'H_25th', 'H_50th', 'H_75th', 'H_100th'] # For results DataFrame
    results = pd.DataFrame(columns = column_names)


    diff_W = 100
    diff_H = 100
    
    global norm_D
    norm_D = np.linalg.norm(N(D))
    
    while abs(diff_W) + abs(diff_H) > threshold or iterations < 100:
        if iterations > max_iter:
            break
            
        W_new = update_W(W, H)
        H_new = update_H(W_new, H)
        
        # Check for nonnegativity
        try: assert(W_new.min() >= 0 and H_new.min() >= 0)
        except AssertionError:
            log.critical('AssertionError: negative entries!')
            log.critical('W_new.min() = %s, H_new.min() = %s', W_new.min(), H_new.min())
            return W_new, H_new, results
            raise

        diff_W = np.linalg.norm(W_new - W)/np.linalg.norm(W)*100
        diff_H = np.linalg.norm(H_new - H)/np.linalg.norm(H)*100

        W, H = W_new, H_new

        # We calculate the relative error in percentage.
        error = calculate_error(D, W, H)
        sparsity = sparsity_metric(H)
        
        
        log.info('Iteration= %s, Error= %s, Sparsity= %s', iterations, error, sparsity)
        iterations += 1
        
        if debug is True:
            H_0th, H_25th, H_50th, H_75th, H_100th = quartiles(H)
            W_minmax = (W.min(), W.max())
        
            column_values = [error, sparsity, diff_W, diff_H, W_minmax,\
                             H_0th, H_25th, H_50th, H_75th, H_100th]

            results = results.append(dict(zip(column_names, column_values)), ignore_index = True)
            
    
    if axing:
        W, H = impose_L1_constraint(W, H)
        W, H = sort_WH(W, H)
    
    error = calculate_error(D, W, H)
    sparsity = sparsity_metric(H)
    log.info('Error= %s, Sparsity= %s', error, sparsity)
    
    return W, H, results[column_names]