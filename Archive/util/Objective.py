''' Objective for matrix factorization
The objective is the error between a matrix D and the product WH.
* we potentially add the L1 norm of H for sparsity
* we normalize the error (and sparsity) by number of entries.  This makes things invariant with respect
to the number of dimensions
'''

import numpy

class Objective:
    '''
    given a matrix D, construct objective function and its gradients corresponding
    to sparce constrained nonnegative matrix factorization
    '''
    def __init__(self, D, N, beta = 0):
        self.beta = beta
        self.D = D
        (self.rows, self.cols) = D.shape
        self.flags = numpy.isnan(D)
        self.OnesNN = numpy.ones((N, N))

    def mask(self, M):
        # D.mask(M) masks the values of M using D.flags.
        out = M.copy()
        out[self.flags] = 0
        return out
        
    def evaluate(self, point):
        (W, H) = point
        Err = self.mask(self.D - W@H)
        (hrows, hcols) = H.shape
        sparsity = numpy.sum(numpy.power(numpy.sum(H, axis = 0)/hcols, 2))/hrows
        dsparsity = self.OnesNN @ H/hcols/hcols/hrows
        val = numpy.trace(Err@Err.T)/self.rows/self.cols + self.beta*sparsity
        dW = -2*Err @ H.T/self.rows/self.cols
        dH = -2*W.T @ Err/self.rows/self.cols + 2*self.beta*dsparsity
        return val, (dW, dH)