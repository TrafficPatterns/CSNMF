'''code to end loop.  Compares values.  **Need to fix up**'''

import numpy

class EndChecker:
    def __init__(self):
        '''
        decide when to end
        '''
        self.threshold = 0.001
        self.W = None
        self.H = None
        self.D= None
        self.DErr=None
        self.val = None
        self.ctr = 1     # counter
        self.max_ctr = 500
        self.min_ctr = 475
    
    def stopFlag(self, val, point, grad):
        '''
        returns true when iteration should end
        '''
        self.ctr += 1

        if (self.val is None):
            '''
            no prior data
            '''
            #print("no prior data")
            self.val = val
            (self.W, self.H) = point
            (self.dW, self.dH) = grad
            self.D=self.W@self.H
            return False #don't stop

        
        (W,H) = point
        D=W@H
        valueStopFlag = (abs(val-self.val) < self.threshold*abs(self.val))
        WStopFlag = (numpy.mean(abs(W-self.W)) < self.threshold*numpy.mean(abs(self.W)))
        HStopFlag = (numpy.mean(abs(H-self.H)) < self.threshold*numpy.mean(abs(self.H)))
        DStopFlag = (numpy.mean(abs(D-self.D)) < self.threshold*numpy.mean(abs(self.D)))
        self.DErr=D-self.D
        print(abs(val-self.val)/abs(self.val),
              numpy.mean(abs(W-self.W))/numpy.mean(abs(self.W)),
              numpy.mean(abs(H-self.H))/numpy.mean(abs(self.H)),
              numpy.mean(abs(D-self.D))/numpy.mean(abs(self.D)))
        if (self.ctr>self.max_ctr):
            #print("val: "+str(self.val))
            '''
            nothing changes much
            '''
            return True # stop
        elif (self.ctr <= self.min_ctr):
            return False
        else:
            self.val = val
            (self.W,self.H) = point
            (self.dW,self.dH) = grad
            self.D=D
            return False # don't stop