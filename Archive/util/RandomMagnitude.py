'''Creates an array of uniform random numbers chosen between high and low values'''

import numpy

class RandomMagnitude:
    low = 0.5
    high = 1
    def __init__(self):
        #numpy.random.seed(0)
        self.counter = 0
        pass
        
    def mag(self):
        self.counter += 1
        numpy.random.seed(self.counter)
        return numpy.random.uniform(self.low, self.high)