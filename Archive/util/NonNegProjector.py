''' Project vector along constraint and along edge, stopping at edge when necessary'''


import numpy

class NonNegProjector:
    def __init__(self, constraints=None, shape=None, Type='A'):
        '''
        project x onto constraint * x = 0
        python naturally write vectors as rows
        math thinks of vectors as columns
        '''
        self.constraints = constraints
        if (self.constraints is not None):
            temp = [numpy.ravel(constraint) for constraint in constraints]
            self.constraints = numpy.matrix(temp)
        print('constraints =',self.constraints)
        self.shape = shape
        self.Type = Type
        
    def proj_ker(self, v, A):
        '''
        project v onto kernel of A
        '''
        temp = A @ A.T
        temp = numpy.linalg.inv(temp)
        proj_range = numpy.array(A.T@temp@A)
        #print("A.shape: "+str(A.shape))
        #print("A: "+str(A))
        #print("proj_range.shape: "+str(proj_range.shape))
        #print("v.shape: "+str(v.shape))
        return v - numpy.dot(proj_range, v)
        
    def newpoint(self, x, v):
        '''
        x is current point
        v is proposed direction
        projects v to move along null space of constraint
        and x to remain nonnegative
        '''
        if ((self.shape is not None) and (x.shape!=self.shape)):
            print("x has bad shape")
            return None
        
        if ((self.shape is not None) and (v.shape!=self.shape)):
            print("v has bad shape")
            return None
        
        x=numpy.ravel(x)
        v=numpy.ravel(v)
        
        # project v to kernel of constraint
        v = v if (self.constraints is None) else self.proj_ker(v, self.constraints)
        
        # indices on edge where v points out of positive orthant
        flags = numpy.logical_and(x == 0, v < 0)  
        edgeindices = flags*1
        # create unit vectors in these directions
        edgematrix = numpy.diag(numpy.ravel(edgeindices))
        flags = numpy.logical_not(numpy.all(edgematrix == 0, axis = 1))
        edgematrix = edgematrix[flags]
        # augment original constraint; add kernel of these directions
        # in other words, move along edge of orthant.
        #print("constraints: "+str(self.constraints))
        #print("edgematrix: "+str(edgematrix))
        if ((self.constraints is not None) and (len(edgematrix)>0)):
            constraints = edgematrix if (self.constraints is None) else numpy.vstack([self.constraints,edgematrix])

            v = self.proj_ker(v, constraints)
        # identify indices where point is in interior but moved point is out of orthant
        flags = numpy.logical_and(x > 0, x+v < 0)
    
        # move until the boundary is hit
        ratio = numpy.divide(x, abs(v))
        ratio[numpy.logical_not(flags)] = numpy.inf
        mina = numpy.min(ratio)
        if (mina == numpy.inf):
            newx = x + v
        else:
            newx = x + mina*v
            
        if (self.shape is not None):
            newx=numpy.reshape(newx,self.shape)
        if (self.Type == 'M'):
            newx=numpy.matrix(newx)
        elif (self.Type == 'P'):
            news=pandas.DataFrame(newx,index=x.index,columns=x.columns)
        return newx