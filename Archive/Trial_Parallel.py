from NMF import NMF
from Data_matrix import replace_placeholder
from Data_tensor import purge_nanlinks
from numpy import array, ones, sum, multiply, nanmean, dot, zeros, arange
from numpy.linalg import norm
from time import time
from itertools import product
from mpi4py import MPI
from joblib import Parallel, delayed


#betas = [10**6+i*10**5 for i in range(10,50,10)] #stepsize = 2
#etas = [10*i for i in range(51,81,5)]
ranks = range(47,53)

#tuples = product(betas, etas)
#processes_needed = len(list(tuples)) # apart from process 0

# This initialization is required by MPI
#V = zeros(1)

#comm = MPI.COMM_WORLD
#size = comm.Get_size()
#process = comm.Get_rank()

time0 = time()

'''
with open('./scratch/Full_Link_Ids.txt', 'r') as filename1:
    line = filename1.readline()
    full_link_ids = [int(i) for i in line.split(',')[:-1]]

with open('./scratch/Full_Link_Data.txt', 'r') as filename2:
    V = []
    for line in filename2:
        if bool(line[:-1]):
            V.append([float(i) for i in line.split(',')])

V = array(V).reshape(len(full_link_ids),365*24)
#print time()-time0
V, nli = purge_nanlinks(V, 30)
#print V.shape

V = replace_placeholder(V, value = nanmean(V.T[0:2]))
'''

V=arange(365*24000).reshape(1000, 365*24)

#For using Joblib
return_values = Parallel(n_jobs=-1, verbose=5)(delayed(NMF)(V.T,rank) for rank in ranks)
errors = zip(*return_values)[2]

print errors

'''
Errors=[]

for rank in ranks:
    W,H,error=NMF(V.T,rank)
    Errors.append((rank,error))


for beta, eta in tuples:
    W, H, beta, eta, error = NMF(V.T, 8, beta, eta)
        #H = array([long(i) for i in H.flatten()]).reshape(H.shape)
        if isnan(error):
                sparsity = nan
        else:
                sparsity = sum([norm((i-max(i))/max(i))/len(i) for i in H.T])/len(H.T)
        print 'beta=',beta,'eta=',eta,'sparsity=',sparsity,'error=',error,'\n'
else:
        pass
'''
