''' * This file lists all the Global variables used in Non-Negative Matrix Factorization.
    * In case any of the Factorization paramaters need to be changed, they should be changed in this file instead of locally within each program.'''

# Global variables
RANK  = 50           # Rank for Matrix Factorization in Phase1.py
TRIPS = True         # False reads travel_traveltimes dataset, True reads trips dataset.
                     # Currently False doesn't work due to bad hyperparameters.    
SEEDED = True        # Determines whether cSNMF.py uses seeded random matrices for W,H or not.
BETA   = 5000        # Higher value penalizes lack of sparsity in H. (See Low_Rank_Manhattan_Traffic.pdf).
                     # If unsure, then set BETA = None
AXING_CUTOFF = 0.4   # Set between 0 and 1. Sets the bottom AXING_CUTOFF fraction of entries in each column of H to 0.


# Fixed global parameters for 2011 traffic dataset
HOURS_IN_YEAR = 8760   # 24*365
TOTAL_LINKS   = 260855 # Total Links aka Road-segments in NYC.
FULL_LINKS    = 2302   # Links with less than 720 hours worth of missing data in a year.
EMPTY_LINKS   = 234892 # Links with less that 720 hours worth of data in a year.
MID_LINKS     = 23661  # TOTAL_LINKS - FULL_LINKS - EMPTY_LINKS

assert MID_LINKS == TOTAL_LINKS - FULL_LINKS - EMPTY_LINKS