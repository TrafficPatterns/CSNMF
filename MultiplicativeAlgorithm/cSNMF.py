''' This contains functions called by cSNMF.ipynb for constrained Sparse Nonnegative Matrix Factorization'''

import numpy as np
import pandas as pd
import math
import logging
import itertools as it
import config

logger = logging.getLogger(__name__)


## Useful matrix operations:
def div(A, B): return np.array([[A[i,j]/B[i,j] if A[i,j]>0 else 0 for j in range(A.shape[1])] for i in range(A.shape[0])])
def d(A):      return np.diag(np.diag(A))  # d(A) replaces all off-diagonal entries of A with 0.
def N(A):      return A*NONZEROS           # NONZEROS is a global variable.


# Calculates sparsity of H as in Low_Rank_Manhattan_Traffic.pdf
def sparsity_metric(H):
    def sparsity_column(column):
        n = len(column)
        assert sum(column) != 0, 'Column of zeros in H. Reduce rank'
        ratio = np.linalg.norm(column, 1)/np.linalg.norm(column, 2)
        return (math.sqrt(n) - ratio)/(math.sqrt(n) - 1)
    return np.mean([sparsity_column(column) for column in H.T])


def calculate_error(D, W, H):
    return np.linalg.norm(D - N(W@H))/NORM_D*100


# Constraint imposed on the columns of W
def impose_L1_constraint(W, H):
    W2 = W/sum(W)
    H2 = np.array([H[i]*sum(W)[i] for i in range(len(H))])
    return W2, H2

# Sets the bottom tail_cutoff fraction of entries in each column of H to 0.
def axe_H(H, tail_cutoff = config.AXING_CUTOFF):
    H2 = H.copy()
    for i, column in enumerate(H.T):
        sorted_column  = sorted(column)
        cumulative_sum = it.accumulate(sorted_column)
        kill_count     = sum(list(cumulative_sum) < tail_cutoff*sum(column))
        H2.T[i]        = np.array([entry if entry >= sorted_column[kill_count] else 0 for entry in column])
    return H2


'''Sort columns of W and rows of H according to the following rules --
   - Give each link a total weight of 1.
   - Distribute that weight across all of its signatures in proportion to the corresponding entry in H.
   - For each signature, calculate popularity to be sum of weights across all links.
   - Sort columns of W in decreasing order of popularity.
   - Sort rows of H based on the same permutaion in order to preserve W@H'''
# Sorting is done so we have a rough intuition for which signatures get used the most.
def sort_WH(W, H):
    weights = H/sum(H)
    signature_popularities = sum(weights.T)
    signature_popularities = sorted(enumerate(signature_popularities),\
                                    key = lambda x : x[1], reverse = True)
    W2 = W.copy()
    H2 = H.copy()
    for i in range(len(signature_popularities)):
        W2[:, i] = W[:, signature_popularities[i][0]]
        H2[i] = H[signature_popularities[i][0]]
        
    assert ( np.linalg.norm(W@H - W2@H2) / np.linalg.norm(W@H) ) < 0.01,\
        'Sorting is not working correctly'
    return W2, H2



def factorize(data_array,
              rank      = 50,
              beta      = None,
              threshold = 0.5,    # algorithm stops if change in W and H is less than threshold % 
              max_iter  = 600,
              seed_W    = None,
              seed_H    = None,
              log       = logger,
              debug     = False,  # if debug, then return runtime-statistics 
              axing     = True,   # if axing, then set small entries in H to zero
              init_W    = None):  # optionally specify W as a numpy array and keep it unchanged in every iteration

    log.info('Rank= %s, Threshold= %s', rank, threshold)

    D = np.nan_to_num(data_array)
    eta = D.max()
    if beta is None:
        beta = len(D.T)/2
    
    global NONZEROS, NORM_D
    NONZEROS = ~ np.isnan(data_array)
    NORM_D   = np.linalg.norm(D)
    
    JNN = np.ones((rank, rank))
    def update_W(W, H):
        Term_1 = D @ H.T 
        Term_2 = N(W @ H) @ H.T + eta*W
        W = div(W * Term_1, Term_2)
        return W

    def update_H(W, H):
        Term_3 = W.T @ D
        Term_4 = W.T @ N(W @ H) + beta*(JNN @ H)
        H = div(H * Term_3, Term_4)
        return H
    

    def quartiles(H):
        H2 = sorted(H.flatten())
        quarter = (rank*D.shape[1]-1)//4
        return H2[0], H2[quarter], H2[quarter*2], H2[quarter*3], H2[-1]

    
    logger.info('Initializing W and H...')
    W_shape = (D.shape[0], rank)
    H_shape = (rank, D.shape[1])
    
    if init_W is None:
        change_W = True
        np.random.seed(seed_W)
        W = np.random.uniform(low = 0.01, high = 1., size = W_shape)    
    else:
        change_W = False
        W = init_W
        
    np.random.seed(seed_H)
    H = np.random.uniform(low = 0.01, high = 1., size = H_shape)    
    log.info('W, H chosen')

    
    iterations   = 0
    column_names = ['error', 'sparsity', 'diff_W', 'diff_H', 'W_minmax',
                    'H_0th', 'H_25th', 'H_50th', 'H_75th', 'H_100th'] # For results DataFrame
    results = pd.DataFrame(columns = column_names)
    diff_W  = 100
    diff_H  = 100
    
    # Run till stopping condition is met
    while abs(diff_W) + abs(diff_H) > threshold or iterations < 100:
        if iterations > max_iter:
            break
            
        if change_W:
            W_new = update_W(W, H)
        else:
            W_new = W
        H_new = update_H(W_new, H)
        
        # Check for nonnegativity
        try: assert(W_new.min() >= 0 and H_new.min() >= 0)
        except AssertionError:
            log.critical('AssertionError: negative entries!')
            log.critical('W_new.min() = %s, H_new.min() = %s', W_new.min(), H_new.min())
            return W_new, H_new, results
            raise

        diff_W = np.linalg.norm(W_new - W)/np.linalg.norm(W)*100
        diff_H = np.linalg.norm(H_new - H)/np.linalg.norm(H)*100

        W, H = W_new, H_new

        # We calculate the relative error in percentage.
        error = calculate_error(D, W, H)
        sparsity = sparsity_metric(H)
        
        
        log.info('Iteration= %s, Error= %s, Sparsity= %s', iterations, error, sparsity)
        iterations += 1
        
        if debug is True:
            H_0th, H_25th, H_50th, H_75th, H_100th = quartiles(H)
            W_minmax = (W.min(), W.max())
            column_values = [error, sparsity, diff_W, diff_H, W_minmax,\
                             H_0th, H_25th, H_50th, H_75th, H_100th]
            results = results.append(dict(zip(column_names, column_values)), ignore_index = True)
    
    
    W, H = impose_L1_constraint(W, H)
    if axing:  H = axe_H(H)
    W, H = sort_WH(W, H)
    
    error = calculate_error(D, W, H)
    sparsity = sparsity_metric(H)
    log.info('Error= %s, Sparsity= %s', error, sparsity)
    
    return W, H, results[column_names]
