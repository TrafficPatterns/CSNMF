#  Logger for writing outputs to .log file.
#  .info prints to standard output + .log file
#  .debug prints to only .log file

import logging
import time
import sys
import config
logdatetime = time.strftime("%m%d")

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.handlers =[]

fh = logging.FileHandler('./'+logdatetime+'.log', mode='w')
fh.setLevel(logging.DEBUG)

sh = logging.StreamHandler(stream=sys.stdout)
sh.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)

logger.addHandler(fh)
logger.addHandler(sh)